class frange:
    """
    Create 'frange' iterator that can work with float.
    """

    def __init__(self, start, stop=None, step=1):
        self._left = start
        self._right = stop
        self._step = step
        if self._right is None:
            self._right = self._left
            self._left = 0

    def __iter__(self):
        return self

    def __next__(self):
        self._left = round(self._left, 15)
        if (self._step > 0 and self._left >= self._right) or (
                self._step < 0 and self._left <= self._right) or self._step == 0:
            raise StopIteration()
        result = self._left
        self._left += self._step
        return result


for i in frange(1, 100, 3.5):
    print(i)


assert (list(frange(5)) == [0, 1, 2, 3, 4])
assert (list(frange(2, 5)) == [2, 3, 4])
assert (list(frange(2, 10, 2)) == [2, 4, 6, 8])
assert (list(frange(10, 2, -2)) == [10, 8, 6, 4])
assert (list(frange(2, 5.5, 1.5)) == [2, 3.5, 5])
assert (list(frange(1, 5)) == [1, 2, 3, 4])
assert (list(frange(0, 5)) == [0, 1, 2, 3, 4])
assert (list(frange(0, 0)) == [])
assert (list(frange(100, 0)) == [])

print('SUCCESS!')

import os
import sqlite3
from typing import List, Tuple


def my_unwrapper(records: List[Tuple]) -> None:
    """
    Разворачивает ответ из БД
    :param records: List of tuples
    :return: None
    """
    for record in records:
        print(*record)


def unique_names_of_customers() -> None:
    """
    Реализовать функцию, которая выведет количество уникальных FirstName из таблицы customers.
    Результатом выполнения будет принт в консоль всех уникальных значений.
    :return: None
    """
    db_pass = os.path.join(os.getcwd(), 'chinook.db')
    conn = sqlite3.connect(db_pass)
    cur = conn.cursor()
    query = '''
    SELECT DISTINCT FirstName
        FROM customers
    '''
    cur.execute(query)
    records = cur.fetchall()
    conn.close()
    my_unwrapper(records)


unique_names_of_customers()


def profit():
    """
    Реализовать функцию, которая выведет прибыль по таблице invoice_items. Сумма по заказу = UnitPrice * Quantity.
    Результат выполнения - принт в консоль одной цифры прибыли.
    :return:
    """
    db_pass = os.path.join(os.getcwd(), 'chinook.db')
    conn = sqlite3.connect(db_pass)
    cur = conn.cursor()
    query = '''
        SELECT  SUM(UnitPrice * Quantity)
            FROM invoice_items
        '''
    cur.execute(query)
    record = cur.fetchall()
    conn.close()
    my_unwrapper(record)


profit()





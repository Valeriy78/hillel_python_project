"""
Realize task 7.1 using 'in'.
Point(1, 2) in Circle(1, 2, 10) -> True or False
p1 in c1 -> True or False
"""


class Point:

    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y


class Circle:

    def __init__(self, x: float, y: float, radius: float):
        self.x = x
        self.y = y
        self.radius = radius

    def __contains__(self, point: Point) -> bool:
        return (self.x - point.x)**2 + (self.y - point.y)**2 <= self.radius**2


p1 = Point(1, 2)
c1 = Circle(1, 2, 10)
print(p1 in c1)

import math


def convert_degrees_to_radians(angle):
    return (math.pi / 180) * angle

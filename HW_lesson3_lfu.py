import functools
import os
import psutil
import requests
from collections import OrderedDict


def cache(max_limit=64):
    """
     Реалізувати LFU алгоритм для кешування. За базу берем існуючий декоратор.
     Написати для фетчування юерелів. Додати можливість указати максимум елементів в кеші.
    """
    
    def internal(f):
        @functools.wraps(f)
        def deco(*args, **kwargs):
            cache_key = (args, tuple(kwargs.items()))
            if cache_key in deco._cache:
                deco._cache[cache_key][1] += 1
                return deco._cache[cache_key][0]
            # else
            result = f(*args, **kwargs)
            if len(deco._cache) >= max_limit:
                # searching for minimal counter
                counters = []
                for item in deco._cache.values():
                    counters.append(item[1])
                min_count = min(counters)
                # move element with minimal counter to the end
                for key in deco._cache:
                    if deco._cache[key][1] == min_count:
                        deco._cache.move_to_end(key, last=True)
                        break
                # delete last element
                deco._cache.popitem(last=True)
            # create new element with counter
            deco._cache[cache_key] = [result, 1]
            return result
        deco._cache = OrderedDict()
        return deco
    return internal


def memory_usage(f):
    """
    Створити декоратор для заміру пам'яті.
    """

    def internal(*args, **kwargs):
        process = psutil.Process(os.getpid())
        memory_before = process.memory_info().rss
        result = f(*args, **kwargs)
        memory_after = process.memory_info().rss
        print(f'Usage of memory by function {f.__name__}: {memory_after - memory_before}')
        return result
    return internal


@memory_usage
@cache(max_limit=5)
def fetch_url(url, first_n=100):
    """Fetch a given url"""
    res = requests.get(url)
    return res.content[:first_n] if first_n else res.content


print(fetch_url('https://google.com'))
print(fetch_url('https://google.com'))
print(fetch_url('https://google.com'))
print(fetch_url('https://ithillel.ua'))
print(fetch_url('https://dou.ua'))
print(fetch_url('https://ain.ua'))
print(fetch_url('https://youtube.com'))

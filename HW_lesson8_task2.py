"""
Create a context manager that will color the output text.
"""

COLORS = {
    'GREY ': '\033[90m',
    'RED': '\033[91m',
    'GREEN': '\033[92m',
    'YELLOW': '\033[93m',
    'BLUE': '\033[94m',
    'PINK': '\033[95m',
    'TURQUOISE': '\033[96m',
    'ENDC': '\033[0m',
}


class colorizer:

    def __init__(self, color):
        self._color = color.upper()

    def __enter__(self):
        print(COLORS[self._color], end='')

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(COLORS['ENDC'], end='')


with colorizer('red'):
    print('printed in red')
print('printed in default color')

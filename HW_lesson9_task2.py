"""
SOLID principles:
1. Single Responsibility Principle
2. Open Closed Principle
3. Liskov Substitution Principle
4. Interface Segregation Principle
5. Dependency Inversion Principle
Realize each of these principles using classes Student, Teacher, Group.
"""


class Person:

    def __init__(self, first_name: str, last_name: str, birth_date: str, email: str):
        self.first_name = first_name
        self.last_name = last_name
        self.birth_date = birth_date
        self.email = email

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def __repr__(self):
        return f"{self.first_name} {self.last_name}"


class Lesson:

    def __init__(self, lesson_number: int, lesson_topic: str):
        self.number = lesson_number
        self.topic = lesson_topic

    def __str__(self):
        return f"'Lesson {self.number} : {self.topic}'"

    def __repr__(self):
        return f"'Lesson {self.number} : {self.topic}'"


class HomeWork:

    def __init__(self, homework_number: int, homework_topic: str):
        self.number = homework_number
        self.topic = homework_topic

    def __str__(self):
        return f"'Homework {self.number} : {self.topic}'"

    def __repr__(self):
        return f"'Homework {self.number} : {self.topic}'"


class Student(Person):

    def __init__(self, first_name, last_name, birth_date, email):
        super().__init__(first_name, last_name, birth_date, email)
        self.group_number = None
        self.lessons_learned = []
        self.homeworks_completed = []
        self.grades = {}

    def learn_lesson(self, lesson: Lesson):
        print(f"Student {self.first_name} {self.last_name} has learned the lesson '{lesson.topic}'")
        if lesson not in self.lessons_learned:
            self.lessons_learned.append(lesson)

    def complete_homework(self, homework: HomeWork):
        print(f"Student {self.first_name} {self.last_name} has completed homework number {homework.number}")
        if homework not in self.homeworks_completed:
            self.homeworks_completed.append(homework)


class Teacher(Person):

    def __init__(self, first_name, last_name, birth_date, email):
        super().__init__(first_name, last_name, birth_date, email)
        self.group_number = None

    def teach(self, lesson: Lesson):
        print(f"Teacher {self.first_name} {self.last_name} teaches the lesson '{lesson.topic}'")

    def check_homework(self, student: Student, homework: HomeWork, grade: int):
        print(f"Teacher {self.first_name} {self.last_name} has checked homework number {homework.number} of "
              f"student  {student.first_name} {student.last_name}")
        student.grades[homework.number] = grade


class StudentCheater(Student):

    def __init__(self, first_name, last_name, birth_date, email):
        super().__init__(first_name, last_name, birth_date, email)

    def grades_hack(self):
        for item in self.grades:
            self.grades[item] = 100
        print("The cheater hacked into the system and gave himself the highest grades!")


class Group:

    def __init__(self, number: int):
        self.number = number
        self.teacher = None
        self.students = []
        self.lessons = []
        self.homeworks = []

    def add_teacher_to_group(self, teacher: Teacher):
        self.teacher = teacher
        teacher.group_number = self.number

    def add_student_to_group(self, student: Student):
        self.students.append(student)
        student.group_number = self.number

    def add_lesson(self, lesson: Lesson):
        self.lessons.append(lesson)

    def add_homework(self, homework: HomeWork):
        self.homeworks.append(homework)
